package com.pixabay.pixabay.ui.activities;

import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.pixabay.pixabay.R;
import com.pixabay.pixabay.ui.fragments.ImagePreviewFragment;
import com.pixabay.pixabay.ui.fragments.InitialFragment;

import java.util.Objects;

import static com.pixabay.pixabay.ui.fragments.ImagePreviewFragment.PERMISSIONS_REQUEST_CODE;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_container,InitialFragment.newInstance())
                .commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            boolean isChecked = true;
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    isChecked = false;
                    break;
                }
            }
            if (isChecked) {
                if(getSupportFragmentManager().findFragmentById(R.id.main_container) != null &&
                        getSupportFragmentManager().findFragmentById(R.id.main_container) instanceof ImagePreviewFragment){
                    ((ImagePreviewFragment) Objects.requireNonNull(getSupportFragmentManager().findFragmentById(R.id.main_container))).saveImage();
                }
            }
        }
    }
}
