package com.pixabay.pixabay.ui.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pixabay.pixabay.R;
import com.pixabay.pixabay.adapters.ImagesAdapter;
import com.pixabay.pixabay.ui.activities.BaseActivity;
import com.pixabay.pixabay.viewmodels.InitialViewModel;

import java.util.Objects;

public class InitialFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView imagesList;
    private SearchView searchView;
    private SwipeRefreshLayout swipeView;

    private InitialViewModel model;
    private ImagesAdapter adapter;

    public static InitialFragment newInstance() {

        Bundle args = new Bundle();
        InitialFragment fragment = new InitialFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_initial, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initUI();
        bindModelData();
        setListeners();
    }

    private void initViews(View view) {
        imagesList = view.findViewById(R.id.images_list);
        searchView = view.findViewById(R.id.search_view);
        swipeView = view.findViewById(R.id.swipe_view);
    }

    private void bindModelData() {

        model = ViewModelProviders.of(this).get(InitialViewModel.class);

        model.getImagesPagedList().observe(this, pagedListResource -> {
            if (pagedListResource != null) {
                switch (pagedListResource.status) {
                    case SUCCESS:
                        ((BaseActivity) Objects.requireNonNull(getActivity())).dismissCurrentDialog();
                        swipeView.setRefreshing(false);
                        break;
                    case ERROR:
                        ((BaseActivity) Objects.requireNonNull(getActivity())).dismissCurrentDialog();
                        swipeView.setRefreshing(false);
                        if (pagedListResource.message != null) {
                            ((BaseActivity) getActivity()).showErrorDialog(pagedListResource.message);
                        } else {
                            ((BaseActivity) getActivity()).showErrorDialog();
                        }
                        break;
                    case LOADING:
                        ((BaseActivity) Objects.requireNonNull(getActivity())).showLoadingDialog();
                        adapter.submitList(pagedListResource.data);
                        break;
                }
            }
        });
    }

    private void initUI() {
        swipeView.setEnabled(true);
        swipeView.setOnRefreshListener(this);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        Objects.requireNonNull(getActivity()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels / 3;
        adapter = new ImagesAdapter(width);
        imagesList.setLayoutManager(new GridLayoutManager(getContext(), 3));
        imagesList.setAdapter(adapter);
    }

    private void setListeners() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                model.getSearchedImages(String.valueOf(searchView.getQuery()));
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        searchView.setOnCloseListener(() -> {
            model.getImagesPagedList();
            return false;
        });

        adapter.setOnImageItemClickListener(image -> {
            ((BaseActivity) getActivity()).showLoadingDialog();
            new Handler().postDelayed(() -> {
                ((BaseActivity) getActivity()).dismissCurrentDialog();
                Objects.requireNonNull(getFragmentManager()).beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.main_container, ImagePreviewFragment.newInstance(image.getId(), image.getFullImageUrl()))
                        .commit();
            }, 2000);
        });
    }

    @Override
    public void onRefresh() {
        swipeView.setEnabled(false);
        new Handler().postDelayed(() -> {
            if (swipeView != null) {
                swipeView.setEnabled(true);
            }
        }, 3000);
        if (!searchView.getQuery().equals("")) {
            model.getSearchedImages(String.valueOf(searchView.getQuery()));
        } else {
            model.getImagesPagedList();
        }
    }
}
