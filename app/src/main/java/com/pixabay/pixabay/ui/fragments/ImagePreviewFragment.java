package com.pixabay.pixabay.ui.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.pixabay.pixabay.R;
import com.pixabay.pixabay.ui.activities.BaseActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Objects;

public class ImagePreviewFragment extends Fragment {
    private static final String IMAGE_URL_ARGUMENT = "ImageUrlArgument";
    private static final String IMAGE_ID_ARGUMENT = "ImageIdArgument";
    public static final int PERMISSIONS_REQUEST_CODE = 17;

    private ImageView previewImage;
    private TextView saveButton;
    private ProgressBar progressBar;
    private Bitmap bitmap;

    public static ImagePreviewFragment newInstance(int imageId,String imageUrl) {

        Bundle args = new Bundle();
        args.putString(IMAGE_URL_ARGUMENT,imageUrl);
        args.putInt(IMAGE_ID_ARGUMENT,imageId);
        ImagePreviewFragment fragment = new ImagePreviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_preview, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initUI();
        setListeners();
    }

    private void initViews(View view){
        previewImage = view.findViewById(R.id.preview_image);
        saveButton = view.findViewById(R.id.save_button);
        progressBar = view.findViewById(R.id.progress_bar);
    }

    private void initUI(){
        Glide.with(Objects.requireNonNull(getContext()))
                .asBitmap()
                .load(Objects.requireNonNull(getArguments()).getString(IMAGE_URL_ARGUMENT))
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        if(e != null) {
                            ((BaseActivity) Objects.requireNonNull(getActivity())).showErrorDialog(e.getMessage());
                        }else {
                            ((BaseActivity) Objects.requireNonNull(getActivity())).showErrorDialog();
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        previewImage.setImageBitmap(resource);
                        bitmap = resource;
                    }
                });
    }

    private void setListeners() {
        saveButton.setOnClickListener(v -> {
            if (checkPermissions() && bitmap != null){
                saveImage();
            }
        });
    }

    private boolean checkPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }

    public void saveImage() {
        String savedImagePath;

        String imageFileName = "JPEG_" + String.valueOf(Objects.requireNonNull(getArguments()).getInt(IMAGE_ID_ARGUMENT)) + ".jpg";
        File storageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                        + "/" + getString(R.string.pixabay));
        boolean success = true;
        if (!storageDir.exists()) {
            success = storageDir.mkdirs();
        }
        if (success) {
            File imageFile = new File(storageDir, imageFileName);
            savedImagePath = imageFile.getAbsolutePath();
            try {
                OutputStream fOut = new FileOutputStream(imageFile);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                fOut.close();
            } catch (Exception e) {
                Toast.makeText(getContext(), getString(R.string.image_not_save), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            // Add the image to the system gallery
            galleryAddPic(savedImagePath);
        }
    }

    private void galleryAddPic(String imagePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(imagePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
        Toast.makeText(getContext(), getString(R.string.image_saved), Toast.LENGTH_SHORT).show();
    }
}
