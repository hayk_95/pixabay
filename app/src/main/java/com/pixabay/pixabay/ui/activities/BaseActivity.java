package com.pixabay.pixabay.ui.activities;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.pixabay.pixabay.R;
import com.pixabay.pixabay.Utils;
import com.pixabay.pixabay.ui.fragments.dialogfragments.LoadingDialogFragment;

public class BaseActivity extends AppCompatActivity {

    public static final String LOADING_TAG = "loading";
    public static final String ERROR_TAG = "error";

    private LoadingDialogFragment loadingDialog;

    private int pendingLoadingCount = 0;


    public void showLoadingDialog() {
        if (getSupportFragmentManager().findFragmentByTag(ERROR_TAG) == null) {
            if (pendingLoadingCount == 0) {
                dismissCurrentDialog();
                loadingDialog = LoadingDialogFragment.newInstance();
                FragmentManager manager = getSupportFragmentManager();
                loadingDialog.show(manager, LOADING_TAG);
            }
            pendingLoadingCount++;
        }
    }

    public void dismissCurrentDialog() {

        if (getSupportFragmentManager().findFragmentByTag(ERROR_TAG) == null) {
            if (getSupportFragmentManager().findFragmentByTag(LOADING_TAG) != null) {
                if (pendingLoadingCount > 0) {
                    pendingLoadingCount--;
                }
                if (pendingLoadingCount == 0) {
                    loadingDialog.dismiss();
                }
            }
        }
    }

    public void showErrorDialog() {
        if (getSupportFragmentManager().findFragmentByTag(ERROR_TAG) == null) {
            String errorMessage = Utils.isNetworkConnected(this)
                    ? getString(R.string.default_error_message)
                    : getString(R.string.no_connection_error_message);
            showErrorDialog(errorMessage);
        }
    }

    public void showErrorDialog(String errorMsg) {
        if (getSupportFragmentManager().findFragmentByTag(ERROR_TAG) == null) {
            dismissCurrentDialog();
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.error))
                    .setMessage(errorMsg)
                    .setPositiveButton(getString(R.string.ok),(dialog, which) -> dialog.dismiss())
                    .create().show();
        }
    }
}
