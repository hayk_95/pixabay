package com.pixabay.pixabay.network.responses;

import com.google.gson.annotations.SerializedName;
import com.pixabay.pixabay.data.models.Image;

import java.util.List;

public class GetImagesResponse {

    @SerializedName("hits")
    private List<Image> list;

    @SerializedName("message")
    private String message;

    public List<Image> getList() {
        return list;
    }

    public void setList(List<Image> list) {
        this.list = list;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
