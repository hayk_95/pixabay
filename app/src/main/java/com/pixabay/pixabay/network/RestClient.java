package com.pixabay.pixabay.network;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class RestClient {
    private final String BASE_URL = "https://pixabay.com/";
    private static final String USER_KEY = "10862783-c09bed4e85cce66f154579d61";

    private static RestClient instance;
    private AsyncHttpClient client;
    private RequestParams params;

    private RestClient(){
        client = new AsyncHttpClient();
        params = new RequestParams();
        params.add("key",USER_KEY);
    }

    public static synchronized RestClient getInstance(){
        if(instance == null){
            instance = new RestClient();
        }
        return instance;
    }

    public void getImages(int page,int pageSize,AsyncHttpResponseHandler callback){
        params.add("page",String.valueOf(page));
        params.add("per_page",String.valueOf(pageSize));
        client.get(BASE_URL + "api/", params, callback);
        params.remove("page");
    }

}
