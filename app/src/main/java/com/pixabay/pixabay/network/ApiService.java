package com.pixabay.pixabay.network;

import com.pixabay.pixabay.network.responses.GetImagesResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("/api/")
    Call<GetImagesResponse> getImages(@Query("key") String key,@Query("page") int page,@Query("per_page") int pageSize);

    @GET("/api/")
    Call<GetImagesResponse> getSearchedImages(@Query("key") String key, @Query("page") int page, @Query("per_page") int pageSize, @Query("q") String filter);
}
