package com.pixabay.pixabay.adapters;

import android.arch.paging.PagedListAdapter;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.pixabay.pixabay.R;
import com.pixabay.pixabay.data.models.Image;

public class ImagesAdapter extends PagedListAdapter<Image,ImagesAdapter.ImageViewHolder> {
    private int viewHolderWidth;
    private OnImageItemClickListener imageItemClickListener;

    public ImagesAdapter(int viewHolderWidth) {
        super(diffCallback);
        this.viewHolderWidth = viewHolderWidth;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ImageViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_view_holder_layout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder imageViewHolder, int i) {
        imageViewHolder.progressBar.setVisibility(View.VISIBLE);
        Glide.with(imageViewHolder.itemView.getContext())
                .setDefaultRequestOptions(new RequestOptions().centerCrop().placeholder(R.color.gray))
                .load(getItem(i).getImageUrl())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        imageViewHolder.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        imageViewHolder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imageViewHolder.image);

        imageViewHolder.image.setOnClickListener(v -> {
            if(imageItemClickListener != null){
                imageItemClickListener.onImageClicked(getItem(i));
            }
        });
    }

    class ImageViewHolder extends RecyclerView.ViewHolder{
        ImageView image;
        ProgressBar progressBar;

        ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.holder_image);
            progressBar = itemView.findViewById(R.id.progress_bar);
            ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
            layoutParams.width = viewHolderWidth;
            layoutParams.height = viewHolderWidth;
            itemView.setLayoutParams(layoutParams);
        }
    }

    private static DiffUtil.ItemCallback<Image> diffCallback = new DiffUtil.ItemCallback<Image>() {
        @Override
        public boolean areItemsTheSame(@NonNull Image image, @NonNull Image t1) {
            return image.getId() == t1.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Image image, @NonNull Image t1) {
            return image.equals(t1);
        }
    };

    public interface OnImageItemClickListener{
        void onImageClicked(Image image);
    }

    public void setOnImageItemClickListener(OnImageItemClickListener imageItemClickListener){
        this.imageItemClickListener = imageItemClickListener;
    }
}
