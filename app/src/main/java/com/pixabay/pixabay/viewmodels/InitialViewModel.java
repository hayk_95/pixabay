package com.pixabay.pixabay.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PagedList;
import android.support.annotation.NonNull;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.pixabay.pixabay.AppDelegate;
import com.pixabay.pixabay.data.datasource.ImageDataSource;
import com.pixabay.pixabay.data.models.Image;
import com.pixabay.pixabay.executors.MainThreadExecutor;
import com.pixabay.pixabay.network.Resource;

import java.util.concurrent.Executors;

public class InitialViewModel extends AndroidViewModel {
    private MutableLiveData<Resource<PagedList<Image>>> liveData;

    public InitialViewModel(@NonNull Application application) {
        super(application);
    }

    public LiveData<Resource<PagedList<Image>>> getImagesPagedList() {
        liveData = new MutableLiveData<>();

        PagedList<Image> pagedList = new PagedList.Builder<>(new ImageDataSource(liveData) {
            @Override
            public void loadData(int page, AsyncHttpResponseHandler callback) {
                ((AppDelegate) getApplication()).getRepository().getImages(page, callback);
            }
        }, getPagedListConfig())
                .setFetchExecutor(Executors.newSingleThreadExecutor())
                .setNotifyExecutor(new MainThreadExecutor())
                .build();
        liveData.setValue(Resource.loading(pagedList));
        return liveData;
    }

    public void getSearchedImages(String filter) {

        PagedList<Image> pagedList = new PagedList.Builder<>(new ImageDataSource(liveData) {
            @Override
            public void loadData(int page, AsyncHttpResponseHandler callback) {
                ((AppDelegate) getApplication()).getRepository().getSearchedImages(page,filter, callback);
            }
        }, getPagedListConfig())
                .setFetchExecutor(Executors.newSingleThreadExecutor())
                .setNotifyExecutor(new MainThreadExecutor())
                .build();
        liveData.setValue(Resource.loading(pagedList));
    }

    private PagedList.Config getPagedListConfig(){
        return new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(60)
                .build();
    }
}
