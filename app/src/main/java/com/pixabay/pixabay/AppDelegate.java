package com.pixabay.pixabay;

import android.app.Application;
import android.content.Intent;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.security.ProviderInstaller;
import com.pixabay.pixabay.data.DataRepository;

public class AppDelegate extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        upgradeSecurityProvider();
    }

    public DataRepository getRepository() {
        return DataRepository.getInstance();
    }

    private void upgradeSecurityProvider() {
        ProviderInstaller.installIfNeededAsync(this, new ProviderInstaller.ProviderInstallListener() {
            @Override
            public void onProviderInstalled() {

            }

            @Override
            public void onProviderInstallFailed(int errorCode, Intent recoveryIntent) {
                GooglePlayServicesUtil.showErrorNotification(errorCode, AppDelegate.this);
            }
        });
    }
}
