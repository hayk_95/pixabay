package com.pixabay.pixabay.data.enums;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}
