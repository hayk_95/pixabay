package com.pixabay.pixabay.data.datasource;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.PageKeyedDataSource;
import android.arch.paging.PagedList;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.pixabay.pixabay.data.models.Image;
import com.pixabay.pixabay.network.Resource;
import com.pixabay.pixabay.network.responses.GetImagesResponse;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public abstract class ImageDataSource extends PageKeyedDataSource<Integer, Image> {
    private static final int STATUS_CODE_SUCCESS = 200;
    private Gson gson;
    private Handler mainHandler;

    private MutableLiveData<Resource<PagedList<Image>>> liveData;

    protected ImageDataSource(MutableLiveData<Resource<PagedList<Image>>> liveData) {
        this.liveData = liveData;
        gson = new Gson();
        mainHandler = new Handler(Looper.getMainLooper());
    }

    public abstract void loadData(int page, AsyncHttpResponseHandler call);

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Image> callback) {
        Runnable runnable = () ->
                loadData(1, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        if (statusCode == STATUS_CODE_SUCCESS) {
                            callback.onResult(getResponseFromByteArray(responseBody).getList(), null, 2);
                            liveData.setValue(Resource.success(null));
                        } else {
                            liveData.setValue(Resource.error(getResponseFromByteArray(responseBody).getMessage(), null));
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        liveData.setValue(Resource.error(error.getMessage(), null));
                    }
                });
        mainHandler.post(runnable);
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Image> callback) {
        Integer key;
        if (params.key - 1 == 0) {
            key = null;
        } else {
            key = params.key;
        }
        Runnable runnable = () ->
                loadData(params.key, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        if (statusCode == STATUS_CODE_SUCCESS) {
                            callback.onResult(getResponseFromByteArray(responseBody).getList(), key);
                            liveData.setValue(Resource.success(null));
                        } else {
                            liveData.setValue(Resource.error(getResponseFromByteArray(responseBody).getMessage(), null));
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        liveData.setValue(Resource.error(error.getMessage(), null));
                    }
                });
        mainHandler.post(runnable);
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Image> callback) {
        Runnable runnable = () ->
                loadData(params.key, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        if (statusCode == STATUS_CODE_SUCCESS) {
                            callback.onResult(getResponseFromByteArray(responseBody).getList(), params.key + 1);
                            liveData.setValue(Resource.success(null));
                        } else {
                            liveData.setValue(Resource.error(getResponseFromByteArray(responseBody).getMessage(), null));
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        liveData.setValue(Resource.error(error.getMessage(), null));
                    }
                });
        mainHandler.post(runnable);
    }

    private GetImagesResponse getResponseFromByteArray(byte[] array) {
        String testV = null;
        try {
            testV = new JSONObject(new String(array)).toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gson.fromJson(testV, GetImagesResponse.class);
    }
}
