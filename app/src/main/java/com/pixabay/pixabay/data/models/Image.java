package com.pixabay.pixabay.data.models;

import com.google.gson.annotations.SerializedName;

public class Image {

    @SerializedName("id")
    private int id;

    @SerializedName("webformatURL")
    private String imageUrl;

    @SerializedName("largeImageURL")
    private String fullImageUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFullImageUrl() {
        return fullImageUrl;
    }

    public void setFullImageUrl(String fullImageUrl) {
        this.fullImageUrl = fullImageUrl;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Image && ((Image) obj).getImageUrl().equals(imageUrl)){
            return true;
        }else {
            return false;
        }
    }
}
