package com.pixabay.pixabay.data;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.pixabay.pixabay.network.RestClient;

public class DataRepository {

    private static DataRepository instance;
    private RestClient client;

    private DataRepository(){
        client = RestClient.getInstance();
    }

    public static synchronized DataRepository getInstance(){
        if(instance == null){
            instance = new DataRepository();
        }
        return instance;
    }

    public void getImages(int page,AsyncHttpResponseHandler call){
        client.getImages(page,60,call);
    }

    public void getSearchedImages(int page, String filter, AsyncHttpResponseHandler call){
        client.getImages(page,60,call);
    }

}
